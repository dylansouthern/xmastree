import json
import threading

import cherrypy
import neopixel
import board
import math
import time
import colorsys
import random

LIGHT_COUNT = 450

pixels = neopixel.NeoPixel(board.D18, LIGHT_COUNT, auto_write=False)
width = 47
step = 1

offset = 0


class LedWorker(threading.Thread):
	def __init__(self):
		super().__init__()
		self.time = 0
		self.shader = SolidColorShader((0, 255, 0))
		self.stop_running = False

	def run(self) -> None:
		prev = time.time()
		while not self.stop_running:
			curr = time.time()
			self.update_lights(curr - prev)
			prev = curr

	def update_lights(self, elapsed_time):
		self.time += elapsed_time
		self.shader.update(elapsed_time)
		for i in range(LIGHT_COUNT):
			pixels[i] = self.shader.get_color(i, self.time)
		pixels.show()


class Shader:
	def get_color(self, led_index, total_time):
		raise NotImplementedError

	def update(self, elapsed_time):
		pass

	def scale(self, color, scale):
		return tuple(scale * c for c in color)

	def add(self, c1, c2):
		return c1[0] + c2[0], c1[1] + c2[1], c1[2] + c2[2]

	def lerp(self, c1, c2, s):
		nc1 = self.scale(c1, 1 - s)
		nc2 = self.scale(c2, s)
		return self.add(nc1, nc2)

	def floor(self, color):
		return tuple(math.floor(c) for c in color)


class SolidColorShader(Shader):
	def __init__(self, color):
		self.color = color

	def get_color(self, led_index, total_time):
		return self.color


class RainbowShader(Shader):
	def __init__(self, width, speed, brightness):
		self.width = width
		self.speed = speed
		self.brightness = brightness

	def get_color(self, led_index, total_time):
		hue = (led_index + (total_time * self.speed)) / self.width % 1
		rgb = colorsys.hsv_to_rgb(hue, 1, self.brightness)
		return tuple(int(v * 255) for v in rgb)


class TwinkleShader(Shader):
	def __init__(self, frequency, lifetime_min, lifetime_max, base_color, twinkle_color):
		self.particles = []
		self.frequency = frequency
		self.lifetime_min = lifetime_min
		self.lifetime_max = lifetime_max
		self.base_color = base_color
		self.twinkle_color = twinkle_color

	def get_color(self, led_index, total_time):
		particles = [particle for particle in self.particles if particle.position == led_index]
		if len(particles) == 0:
			return self.base_color
		else:
			b = particles[0].get_brightness()
			return self.floor(self.lerp(self.base_color, self.twinkle_color, b))

	def update(self, elapsed_time):
		# TODO: fix aliasing
		for particle in self.particles:
			particle.update(elapsed_time)
		self.particles = [particle for particle in self.particles if not particle.is_dead()]
		if random.random() < (self.frequency * elapsed_time):
			self.particles.append(
				Particle(random.randint(0, LIGHT_COUNT - 1), random.uniform(self.lifetime_min, self.lifetime_max)))


class Particle:
	def __init__(self, position, lifetime):
		self.position = position
		self.age = 0
		self.lifetime = lifetime

	def update(self, elapsed_time):
		self.age += elapsed_time

	def is_dead(self):
		return self.age > self.lifetime

	def get_brightness(self):
		progress = self.age / self.lifetime
		return math.sin(progress * math.pi)


class XmasTreeService(object):
	def __init__(self, worker):
		self.worker = worker

	def get_body(self):
		cl = cherrypy.request.headers['Content-Length']
		raw_body = cherrypy.request.body.read(int(cl))
		return json.loads(raw_body)

	def dict_to_color(self, data):
		return data['red'], data['green'], data['blue']

	@cherrypy.expose
	def solid_color(self, red, green, blue):
		color = (int(red), int(green), int(blue))
		self.worker.shader = SolidColorShader(color)

	@cherrypy.expose
	def rainbow(self, width, speed, brightness):
		self.worker.shader = RainbowShader(float(width), float(speed), float(brightness))

	@cherrypy.expose
	def twinkle(self):
		body = self.get_body()
		base_color = self.dict_to_color(body["base_color"])
		twinkle_color = self.dict_to_color(body["twinkle_color"])
		frequency = float(body["frequency"])
		lifetime_min = float(body['lifetime_min'])
		lifetime_max = float(body['lifetime_max'])
		self.worker.shader = TwinkleShader(frequency, lifetime_min, lifetime_max, base_color, twinkle_color)

	@cherrypy.expose
	def candy_cane(self):
		body = self.get_body()
		color1 = (body['color1']['red'], body['color1']['green'], body['color1']['blue'])
		color2 = (body['color2']['red'], body['color2']['green'], body['color2']['blue'])
		self.run_loop(self.candycane, color1, color2)

	def candycane(self, elapsed_time, color1, color2):
		global offset
		for i in range(150):
			if math.floor((i + offset) / width) % 2 == 0:
				color = color1
				if math.floor(i + offset) % width == 0:
					pass
				# color = self.lerp(color1, color2, offset % 1)
			else:
				color = color2
				if math.floor(i + offset) % width == 0:
					pass
				# color = self.lerp(color2, color1, offset % 1)
		# pixels[i] = self.floor_color(color)
		offset = (offset + (step * elapsed_time))
		pixels.show()

	def run_loop(self, callback, color1, color2):
		prev = time.time()
		while True:
			curr = time.time()
			callback(curr - prev, color1, color2)
			prev = curr


class HelloWorld:
	def index(self):
		return 'Hello World!'

	index.exposed = True


def main():
	worker = LedWorker()
	worker.start()
	cherrypy.config.update({'server.socket_host': '10.0.0.109'})
	cherrypy.quickstart(XmasTreeService(worker))
	worker.stop_running = True


if __name__ == '__main__':
	main()
